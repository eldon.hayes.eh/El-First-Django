# base image
FROM python:3.9
# setup env var
ENV DockerHome=/home/app/webapp

# set work directory
RUN mkdir -p $DockerHome

# where code lives
WORKDIR $DockerHome

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip

# copy whole project to  docker home dir
COPY . $DockerHome

# Run this command to install all dependencies
RUN pip install -r requirements.txt

# port where Django app runs
EXPOSE 8000

# start server
CMD python manage.py runserver_plus --cert-file cert.pem --key-file key.pem 0:8000
