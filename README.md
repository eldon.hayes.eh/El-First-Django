First Django project based on the existing Django tutorial project at https://docs.djangoproject.com/en/4.0/intro/tutorial01/.

As according to the tutorial, this app is a simple poll application with two different aspects to it: A public site that lets people view polls and vote in them. An admin site that lets you add, change, and delete polls.

Dependencies include Python, Django, django-extensions, Werkzeug, and pyOpenSSL. Also, a local SSL certificate will be required to run this in HTTPS.

Beyond the tutorial, further work done involves utilizing it with Github, addressing security concerns by implementing HTTPS to work with it (https://timonweb.com/django/https-django-development-server-ssl-certificate/) with other security options, and following through the deployment checklist offered (https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/).

The project was also Dockerized (https://blog.logrocket.com/dockerizing-django-app/). Beyond that, notable variables were replaced with Protected variables from Gitlab.

Lastly, a very rudimentary pipeline was made that includes OAST/SCA (Safety), SAST (Bandit), and Secrets Scanning (trufflehog) implemented through Docker images of each of those. All tools were tested locally first and made to output results. Images were scanned using docker scanner as well which revealed the discovery of numerous vulnerabilities in each tool's image, some critical, which raises the issue of utilizing these tools without first addressing or considering those issues.
